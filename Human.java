package com.company;

public class Human {

    private final String firstName;
    private final String secondName;
    private final String middleName;

    public Human(String firstName, String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = null;
    }

    public Human(String firstName, String secondName, String middleName) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = middleName;
    }

    public String getShortName() {
        char firstLetterName = firstName.charAt(0);
        if (middleName != null && middleName.length() > 0) {
            char middleLetterName = middleName.charAt(0);
            return secondName + " " + firstLetterName + ". " + middleLetterName + ".";
        } else {
            return secondName + " " + firstLetterName + ". ";
        }
    }

    public String getFullName() {
        if (middleName != null && middleName.length() > 0) {
            return secondName + " " + firstName + " " + middleName;
        } else {
            return secondName + " " + firstName;
        }
    }
}
