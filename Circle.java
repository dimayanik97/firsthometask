package com.company;

public class Circle {

    private int сirclePoint_X;
    private int сirclePoint_Y;
    private int radiusCircle;

    public Circle(int circlePoint_X, int circlePoint_Y, int radiusCircle) {
        this.сirclePoint_X = circlePoint_X;
        this.сirclePoint_Y = circlePoint_Y;
        this.radiusCircle = radiusCircle;
    }

    public void setCirclePoint_X(int circlePoint_X) {
        circlePoint_X = circlePoint_X;
    }

    public void setCirclePoint_Y(int circlePoint_Y) {
        circlePoint_Y = circlePoint_Y;
    }

    public void setRadiusCircle(int radiusCircle) {
        this.radiusCircle = radiusCircle;
    }

    public int getCirclePoint_X() {
        return circlePoint_X;
    }

    public int getCirclePoint_Y() {
        return circlePoint_Y;
    }

    public int getRadiusCircle() {
        return radiusCircle;
    }
}
