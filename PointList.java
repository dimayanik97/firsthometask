package com.company;


import java.util.ArrayList;
import java.util.List;

public class PointList {

        List<Point> points= new ArrayList<Point>();

        public void updatePoints(Point point) {
                points.add(point);
        }

        public List<Point> getPoints() {
                return points;
        }

}
