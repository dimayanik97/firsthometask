package com.company;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        System.out.println("\n-------------------|ДЗ 1, Задача 1|--------------------");

        Human firstHuman = new Human("Иван", "Пупкин", "Василиевич");

        System.out.println("\nКороткое имя: " + firstHuman.getShortName());
        System.out.println("Полное имя: " + firstHuman.getFullName());

        Human secondHuman = new Human("Дмитрий", "Янковский", "");

        System.out.println("\nКороткое имя без отчества: " + secondHuman.getShortName());
        System.out.println("Полное имя без отчества: " + secondHuman.getFullName());

        System.out.println("\n-------------------|ДЗ 1, Задача 2|--------------------");

        Scanner scanner = new Scanner(System.in);

        int indexPoint = 0;

        PointList pointList = new PointList();

        do {
            int temp = 1;
            if (indexPoint >= 1) {
                System.out.print("\nХотите создать еще одну точку? (Да = 1 | Нет = 2)\nОтвет: ");
                temp = scanner.nextInt();
            }
            if (temp == 1) {
                indexPoint++;
                Point point = new Point(0, 0);
                System.out.print("\nВведите координаты " + indexPoint + "-й(ой) точки:\nX = ");
                point.setPointX(scanner.nextInt());
                System.out.print("Y = ");
                point.setPointY(scanner.nextInt());
                pointList.updatePoints(point);
            } else if (temp > 2 || temp <= 0) {
                System.out.print("\nВы ввели неправильный ответ!\n");
            } else {
                break;
            }
        } while (true);

        Circle circle = new Circle(0, 0, 0);

        System.out.print("\nВведите координаты центра окружности:\nX = ");
        circle.setCirclePoint_X(scanner.nextInt());
        System.out.print("Y = ");
        circle.setCirclePoint_Y(scanner.nextInt());
        System.out.print("\nВведите длину радиуса окружности:\nЗначение = ");
        circle.setRadiusCircle(scanner.nextInt());

        List<Point> pointArray = pointList.getPoints();

        String result = "";

        for (int a = 0; a < pointArray.toArray().length; a++) {

            double circlePointResult = Math.sqrt((pointArray.get(a).getPointX() - circle.getCirclePoint_X()) * 2 +
                    (pointArray.get(a).getPointY() - circle.getCirclePoint_Y()) * 2);

            if (circlePointResult < circle.getRadiusCircle()) {
                result += "\n" + (a + 1) + "-я точка лежит в окружности!";
            } else {
                result += "\n" + (a + 1) + "-я точка не лежит в окружности!";
            }
        }

        System.out.println(result);
        System.out.println("\n-------------------------------------------------------");

    }
}


